PREFIX ?= /usr/local
CC ?= cc
LDFLAGS = -lX11
PLATFORM ?= Generic

output: dwmblocks.c blocks.def.h blocks.h
	${CC}  dwmblocks.c $(LDFLAGS) -O3 -march=native -mtune=native -o dwmblocks
blocks.h:
	cp blocks.def.h $@


clean:
	rm -f *.o *.gch dwmblocks

install: output
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f dwmblocks $(DESTDIR)$(PREFIX)/bin/
	chmod 0755 $(DESTDIR)$(PREFIX)/bin/dwmblocks
	cp -f scripts/* $(DESTDIR)$(PREFIX)/bin/
	chmod 0755 $(DESTDIR)$(PREFIX)/bin/dwmblocks-status-*

	if test "Arch" = "$(PLATFORM)"; \
	then \
		mkdir -p $(DESTDIR)/etc/pacman.d/hooks; \
		cp -f hooks/update-status-bar.hook $(DESTDIR)/etc/pacman.d/hooks/; \
		chmod 0644 $(DESTDIR)/etc/pacman.d/hooks/update-status-bar.hook; \
	else \
		echo "update-status-bar hook not installed"; \
	fi

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/dwmblocks
	rm -f $(DESTDIR)$(PREFIX)/bin/dwmblocks-status-*

	if test "true" = "$(ARCH)"; \
	then rm -f $(DESTDIR)/etc/pacman.d/hooks/update-status-bar.hook; \
	fi
