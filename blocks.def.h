// Modify this file to change what commands output to your statusbar, and
// recompile using the make command. Don´t use the signals 2, 15 and anything
// between 34 - 64 To trigger and signal here send a "kill -(signal+34) $(pidof
// dwmblocks)" eg. to trigger signal 3 -> kill -37 $(pidof dwmblocks)
static const Block blocks[] = {
    /*Icon*/ /*Command*/ /*Update Interval (ms)*/ /*Update Signal*/
    {"", "dwmblocks-status-updates", 3600000, 4},
    {"", "dwmblocks-status-volume", 0, 3},
    {"^C03^", "dwmblocks-status-nettraf", 4000, 0},
    {"", "dwmblocks-status-cpu", 5000, 0},
    {"^C13^󰍛 ", "dwmblocks-status-memory", 5000, 0},
    {"^C14^ ", "dwmblocks-status-date", 5000, 0},
};

// sets delimeter between status commands. NULL character ('\0') means no
// delimeter.
static char delim[] = "  ";
static unsigned int delimLen = 4;
